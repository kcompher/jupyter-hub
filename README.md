# BizOps JupyterHub Deployment

This repo provides an easy way to deploy [JupyterHub](https://github.com/jupyterhub/jupyterhub) with [JupyerLab](https://github.com/jupyterlab/jupyterlab), using [GitLab CI/CD](https://docs.gitlab.com/ee/ci/), to [Kubernetes](https://kubernetes.io/).

## Installation

1. Import this project
1. [Connect a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/)
1. Deploy [Tiller & Ingress](https://docs.gitlab.com/ee/user/project/clusters/#installing-applications)
1. When the Ingress has finished, copy the IP address
1. Create a DNS entry which resolves to the Ingress IP
1. [Add an application](https://docs.gitlab.com/ee/integration/oauth_provider.html) to GitLab for Hub to use for OAuth. Requires `api` rights.
1. Define the required [project variables](#project-variables)

### Project Variables

| Variable Name | Description |
| ------------- | ----------- |
| LOADBALANCER_IP | IP address of the Ingress |
| HUB_HOSTNAME  | DNS entry for Hub that resolves to `LOADBALANCER_IP` |
| LEGO_EMAIL | The email address to use for registration with Let's Encrypt |
| LEGO_URL | Let's Encrypt API URL. Use `https://acme-v01.api.letsencrypt.org/directory` for valid certificates |
| OAUTH_CALLBACK_URL | Typically https://`HUB_HOSTNAME`/hub/oauth_callback |
| OAUTH_CLIENT_ID | OAuth Application ID created within GitLab for Hub |
| OAUTH_SECRET | OAuth Application Secret created within GitLab for Hub |
| PROXY_TOKEN | Randomly generated security token. Create with `openssl rand -hex 32` |
| GITLAB_HOST | Hostname of the GitLab server |

## Authentication

Access control is provided in two steps:
1. Users must authenticate with GitLab through OAuth
1. An optional whitelist is available to further restrict access

Admin authentication to manage users and notebook servers is also avaialble by whitelisting specific usernames as administrators.

### Authentication via GitLab with OAuth

Set up GitLab OAuth authentication by creating a new application for Hub: https://docs.gitlab.com/ee/integration/oauth_provider.html Pass the generated `client id` and `secret` as to Hub [project variables](#project-variables).

### Restricting access by the whitelist

JupyterHub supports further restricting access with a simple whitelist. You can manage this by directly editing `hub-values.yaml.template` under:

```yaml
hub:
  auth:
    whitelist:
      - username1
```

### Administration Authentication

Similar to the whitelist, users can be flagged as admins by listing them in the `hub-values.yaml.yemplate` file under:

```yaml
hub:
  auth:
    admin:
      - username1
```

## Getting started

A [sample notebook](Getting Started.md) is available which connects to the BizOps database, runs a query, and then outputs the results in both tabular and chart form.

## Customizing the default notebook

A default notebook is provided with JupyterLab enabled by default, as well as common Python libraries. This image can be changed by editing the `images/notebook/Dockerfile` Dockerfile.

### iPython kernel

The standard iPython notebook is available, with the following packages pre-installed:

```
agate
beautifulsoup4
cufflinks
engarde
emcee
folium
gensim
ipython-sql
jsonify
keras
lifelines
lifetimes
matplotlib
networkx
nltk
numexpr
numpy
pandas
pandasql
patsy
plotly
psycopg2-binary
prettytable
prophet
requests
scikit-image
scikit-learn
scipy
seaborn
SexMachine
statsmodels
sympy
tabulate
tensorflow
textblob
ua-parser
wordcloud
```

### iRuby kernel

An iRuby kernel is also available, for those who are more familiar with Ruby than Pythong. It includes the full Ruby scikit package, including:
```
ai4r - Artificial intelligence
algorithms - Algorithms and data structures
awesome_print - Pretty object printing
bibsync - Bibliography synchronization
bibtex-ruby - BibTeX parser
bio - Bioinformatics suite
cassowary - Incremental constraint solver
classifier-reborn - Machine learning
ctioga2 - Command line tool for tioga
daru - Dataframe library
darwinning - Genetic algorithms
decisiontree - Machine learning
distribution - Statistical distributions
extendmatrix - Helper methods for Matrix
gga4r - Genetic algorithms
gimuby - Genetic algorithms
gnuplot - Plotting library
gnuplotrb - Plotting library
gphys - Grided physical data
gruff - Plotting library
gsl - Bindings for the GNU scientific library
hamster - Efficient persistent data structures
histogram - Histogram method
integration - Numerical integration
irtruby - Machine learning
iruby - Ruby kernel for Jupyter
liblinear-ruby - Linear classifier
lpsolve - Linear solver
mathgl - Plotting library
mdarray - Numerical matrix library
measurable - Distance measures for machine learning
mikon - Dataframe library
minimization - Numerical minimization
mixed_models - Fit statistical models
modshogun - Machine learning
narray - Numerical matrix library
nmatrix - Numerical matrix library
nyaplot - Plotting library
octave-ruby - Interface to Octave
ode - Ordinary differential equations
omoikane - Machine learning
parallel - Parallel processing
phys-units - Calculation with units
plotrb - Plotting library
plplot - PLplot bindings
pry - Ruby shell
publisci - Publishing toolkit
rb-libsvm - Support vector machines (machine learning)
rbczmq - Needed by iruby
rbmetis - Graph partitioning
rinruby - R interpreter
rsemantic - Machine learning
rserve-client - R interpreter
rsruby - R interpreter
ruby-band - Data mining
ruby-dcl - Plotting
ruby-fann - Neural networks
ruby-fftw3 - Fast Fourier transformation
ruby-minisat - SAT solver
ruby-netcdf - Data file
ruby-opencv - Computer vision
ruby-pgplot - Plotting
rubyvis - Plotting library
statsample - Statistics
statsample-bivariate-extension - Statsample extension (Autoloaded)
statsample-glm - Statistics
statsample-optimization - Statsample extension (Autoloaded)
statsample-sem - Statistics
statsample-timeseries - Statistics
stuff-classifier - Machine learning
symbolic - Symbolic math
symengine - Symbolic math
tioga - Scientific plots with PDF and TeX
treat - Natural language processing
unit - Calculation with units
```
